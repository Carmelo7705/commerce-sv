const express = require('express');
const router = express.Router();
const upload = require('../file/file');
const uploadProduct = require('../file/product');

const { auth__Admin: middlewareAdmin, auth_client: middlewareClient } 
    = require('../middlewares/auth');

const {
    UsersController,
    RolesController,
    CategoriesController,
    ProductsController,
    CommentsController,
    RatingsController,
    SellersController
} = require('../controllers');

/** Users Module */
router.get('/users', middlewareAdmin, UsersController.index);
router.post('/login', UsersController.login);
router.post('/register', UsersController.register);
router.get('/user/:id', middlewareClient, UsersController.getUser);
router.put('/user/update/:id', middlewareAdmin, UsersController.updateUser);
router.get('/user/active/:id', middlewareAdmin, UsersController.setActiveUser);
router.get('/user/inactive/:id', middlewareAdmin, UsersController.setInactiveUser);
router.put('/user/update-img/:id', middlewareClient, upload.single('avatar'), UsersController.setImgProfile);

/** Roles Module */
router.get('/roles', middlewareAdmin, RolesController.index);
router.post('/roles', middlewareAdmin, RolesController.store);
router.get('/role/:id', middlewareAdmin, RolesController.show);
router.put('/role/update/:id', middlewareAdmin, RolesController.update);
router.delete('/role/:id', middlewareAdmin, RolesController.destroy);

/** Categories Module */
router.get('/categories', middlewareClient, CategoriesController.index);
router.post('/categories', middlewareAdmin, CategoriesController.store);
router.get('/category/:id', middlewareClient, CategoriesController.show);
router.put('/categories/update/:id', middlewareAdmin, CategoriesController.update);
router.delete('/category/:id', middlewareAdmin, CategoriesController.destroy);

/** Products Module */
router.get('/products', middlewareClient, ProductsController.index);
router.post('/products', middlewareAdmin, ProductsController.store);
router.get('/product/:id', middlewareClient, ProductsController.show);
router.put('/products/update/:id', middlewareAdmin, ProductsController.update);
router.delete('/product/:id', middlewareAdmin, ProductsController.destroy);
router.put('/product/add-image/:id', middlewareAdmin, uploadProduct.single('pic'), ProductsController.uploadImage);
router.delete('/product/delete-images/:id', middlewareAdmin, ProductsController.deleteImages);

/** Comments Module */
router.post('/set-comment', middlewareClient, CommentsController.store);
router.get('/comment-approve/:id',middlewareClient, CommentsController.aprovedComment);
router.get('/comment-hide/:id', middlewareAdmin, CommentsController.hideComment);

/** Rating Module */
router.post('/set-rating', middlewareClient, RatingsController.store);

/** Seller Module */
router.post('/exec-seller', middlewareClient, SellersController.store);
router.post('/products-seller-filter', middlewareClient, SellersController.getSellersProductsFiltered);
router.post('/sellers-month', middlewareAdmin, SellersController.getSellersByMonth);

module.exports = router;