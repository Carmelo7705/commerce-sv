const { Comments } = require('../models');

const store = async (req, res, next) => {
    const { product_id, user_id, description } = req.body;

    const data = {
        product_id,
        user_id,
        description,
        status: 0 // 0 Comment not aproved, 1 comment aproved.
    }

    try {
        await Comments.create(data);

        return res.status(200).json({
            success: true,
            data: 'Thanks for supplied your comment.'
        });
    } catch (error) {
        res.status(400).json({
            err: error
        });
        next(error);
    }
}

const aprovedComment = async (req, res, next) => {
    const { id } = req.params;

    try {
        await Comments.update(
            { status: 1 },
            { where: { id } }
        );

        return res.status(200).json({
            success: true,
            data: 'Has marked this comment public.'
        });

    } catch (error) {
        res.status(400).json({
            err: error
        });
        next(error);
    }
}

const hideComment = async (req, res, next) => {
    const { id } = req.params;

    try {
        await Comments.update(
            { status: 0 },
            { where: { id } }
        );

        return res.status(200).json({
            success: true,
            data: 'Has marked this comment hide.'
        });

    } catch (error) {
        res.status(400).json({
            err: error
        });
        next(error);
    }
}

module.exports = {
    store,
    aprovedComment,
    hideComment
}