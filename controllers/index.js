const UsersController = require('./UsersController');
const RolesController = require('./RolesController');
const CategoriesController = require('./CategoriesController');
const ProductsController = require('./ProductsController');
const CommentsController = require('./CommentsController');
const RatingsController = require('./RatingsController');
const SellersController = require('./SellersController');

module.exports = {
    UsersController,
    RolesController,
    CategoriesController,
    ProductsController,
    CommentsController,
    RatingsController,
    SellersController
};