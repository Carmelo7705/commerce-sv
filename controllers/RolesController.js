const { Roles } = require('../models/');

const index = async (req, res, next) => {
    
    try {
        const roles = await Roles.findAll();

        return res.status(200).json({
            success: true,
            data: roles,
            message: 'List roles availables.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const store = async (req, res, next) => {
    const { name } = req.body;

    try {
        const roles = await Roles.create({ name });

        return res.status(200).json({
            success: true,
            data: roles,
            message: 'Stored role successfully.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const show = async (req, res, next) => {
    const { id } = req.params;

    try {
        const roles = await Roles.findOne({ where: { id } });

        return res.status(200).json({
            success: true,
            data: roles,
            message: 'Role by id'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const update = async (req, res, next) => {
    const { name } = req.body;
    const { id } = req.params;

    try {
        const roles = await Roles.update(
            { name },
            { where: { id } }
        );

        return res.status(200).json({
            success: true,
            data: roles,
            message: 'update roles.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const destroy = async (req, res, next) => {
    const { id } = req.params;

    try {
        const roles = await Roles.destroy(
            { where: { id } }
        );

        return res.status(200).json({
            success: true,
            data: roles,
            message: 'Role deleted.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

module.exports = {
    index,
    store,
    show,
    update,
    destroy
}