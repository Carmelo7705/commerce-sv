const { Category } = require('../models/');

const index = async (req, res, next) => {
    
    try {
        const categories = await Category.findAll();

        return res.status(200).json({
            success: true,
            data: categories,
            message: 'List categories availables.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const store = async (req, res, next) => {
    const { name } = req.body;

    try {
        const categories = await Category.create({ name });

        return res.status(200).json({
            success: true,
            data: categories,
            message: 'Stored category successfully.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const show = async (req, res, next) => {
    const { id } = req.params;

    try {
        const category = await Category.findOne({ where: { id } });

        return res.status(200).json({
            success: true,
            data: category,
            message: 'Category by id'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const update = async (req, res, next) => {
    const { name } = req.body;
    const { id } = req.params;

    try {
        const categories = await Category.update(
            { name },
            { where: { id } }
        );

        return res.status(200).json({
            success: true,
            data: categories,
            message: 'update category.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const destroy = async (req, res, next) => {
    const { id } = req.params;

    try {
        const category = await Category.destroy(
            { where: { id } }
        );

        return res.status(200).json({
            success: true,
            data: category,
            message: 'Category deleted.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

module.exports = {
    index,
    store,
    show,
    update,
    destroy
}