const { Sequelize } = require('sequelize');
const { Seller, ProductsSeller, User } = require('../models');

const store = async (req, res, _) => {
    const { user_id, products } = req.body;

    const total = getTotal(products);

    const seller = await setSeller( user_id, total );

    if(seller.success) {
        await setProductSellers(products, seller);

        return res.status(200).json({
            success: true,
            message: 'Has buyed this products successfully.'
        });
    }
    
    return res.status(200).json({
        success: false,
        message: 'Error',
        err: seller.err
    });

}

const getSellersProductsFiltered = async (req, res, next) => {
    const { date_init, date_end, product_id } = req.body;

    let conditions = {};

    console.log(date_init, date_end);

    if(date_init != '' && date_end != '') {
        const end_date = convertDateEnd(date_end);

        conditions = {
            createdAt: {
                [Sequelize.Op.between]: [date_init, end_date]
            }
        }
    }

    if(product_id != '') {
        conditions = {
            ...conditions,
            product_id
        }
    }

    try {
        const productSells = await ProductsSeller.findAll({ where: conditions });

        return res.status(200).json({
            success: true,
            data: productSells,
            message: 'List products seller filter.'
        });

    } catch (error) {
        res.status(400).json({
            success: false,
            err: error,
            message: 'has error ocurred'
        });

        next(error)
    }
}

const getSellersByMonth = async (req, res, next) => {
    const { month } = req.body;

    try {
        const sellers = await Seller.findAll({
            attributes: [
                'total_price',
                'status',
                [Sequelize.fn('date_format', Sequelize.col('seller.createdAt'), '%Y-%m-%d'), 'date']
            ],
            include: [ { as: 'users', model: User } ],
            where: Sequelize.where(
                Sequelize.fn('month', Sequelize.col('seller.createdAt')), month
            ),
        });

        return res.status(200).json({
            success: true,
            data: sellers,
            message: 'List sellers filter by month.'
        });

    } catch (error) {
        res.status(400).json({
            success: false,
            err: error,
            message: 'has error ocurred'
        });

        next(error)
    }
}

const setSeller = async (user_id, total) => {
    try {
        const seller = await Seller.create({
            user_id,
            total_price: total,
            status: 0
        });

        return {
            success: true,
            id: seller.id
        }

    } catch (error) {
        return {
            success: false,
            err: error
        }
    }
}

const setProductSellers = async (products, seller) => {
    try {
        await Promise.all(
            products.forEach( async (product) => {
                await ProductsSeller.create({
                    product_id:product.product_id,
                    seller_id: seller.id,
                    price: product.price,
                    quantity: product.quantity
                });
            })
        );

        return {
            success: true
        }
    } catch (error) {
        return {
            success: false,
            err: error
        }
    }
}

const getTotal = (products) => {
    let total = 0;

    products.forEach(product => {
        total += product.price * product.quantity;
    });

    return total;
}

const convertDateEnd = (date) => {
    const formatEnd = new Date(date);
    const end = formatEnd.setDate(formatEnd.getDate() + 1);
    let finalend = new Date(end).toISOString().split('T', 2)[0];

    return finalend;
}

module.exports = {
    store,
    getSellersProductsFiltered,
    getSellersByMonth
}