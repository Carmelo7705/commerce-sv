const { User } = require('../models/');
const bcrypt = require('bcryptjs');
const fs = require('fs');

const index = async (req, res, next) => {
    try {
        const users = await User.findAll();
        return res.status(200).json({ data: users }); 
    } catch (error) {
        res.status(500).json({
            message: 'Error ocurred.',
            error: error
        });
        next(error);
    }
}

const login = async (req, res, next) => {
    const { email, password } = req.body;

    if(!email || !password) {
        return res.status(400).json({
            message: 'Request missing Email or Password param!'
        });
    }

    try {
        const user = await User.findOne({ where: { email } });

        if(!user) {
            return res.status(400).json({
                message: 'Email not found.'
            });
        }

        let match = bcrypt.compareSync(password, user.password);

        if (!match) {
            return res.status(400).json({
                message: 'Password do no match.'
            });
        }

        const token = await User.createToken(user);
        const refreshToken = await User.refreshToken(user);

        return res.status(200).json({
            success: true,
            token,
            refreshToken,
            message: 'Login successfully.'
        });

    } catch (error) {
        res.status(400).send('Invalid username or password');
        next(error);        
    }
}

const register = async (req, res, next) => {
    const { firstname, lastname, email, role } = req.body;

    const password = await bcrypt.hash( req.body.password, 10 );

    const data = {
        firstname,
        lastname,
        email,
        password,
        role
    };

    try {
        const user = await User.create(data);

        return res.status(200).json({
            success: true,
            message: 'User created successfully.',
            data: user
        });

    } catch (error) {
        res.status(400).json('Error ocurred in user stored.');
        next(error);
    }
}

const getUser = async (req, res, next) => {
    const { id } = req.params;

    try {
        const user = await User.findOne({ where: { id: id } });

        return res.status(200).json({
            success: true,
            message: 'User by ID',
            data: user
        });
    } catch (error) {
        res.status(400).json('Error ocurred in user get.');
        next(error);
    }
}

const updateUser = async (req, res, next) => {
    const { firstname, lastname, email, password, role } = req.body;
    const { id } = req.params;

    const user = await User.findOne({ where: { id: id } });

    let psw = password !== '' ? await bcrypt.hash( password, 10 ) : user.password;

    const data = {
        firstname,
        lastname,
        email,
        password: psw,
        role
    }

    try {
        const updt = await User.update(
            data,
            { where: { id: id } }
        );

        return res.status(200).json({
            success: true,
            message: 'User updated successfully.',
        });
    } catch (error) {
        res.status(400).json('Error ocurred in user updated.');
        next(error);
    }
}

const setActiveUser = async (req, res, next) => {
    const { id } = req.params;

    try {
        const user = await User.update(
            { status: 1 },
            { where: { id: id } }
        );

        return res.status(200).json({
            success: true,
            message: 'User actived successfully.',
            data: user
        });
    } catch (error) {
        res.status(400).json('Error ocurred in user actived.');
        next(error);
    }
}

const setInactiveUser = async (req, res, next) => {
    const { id } = req.params;

    try {
        const user = await User.update(
            { status: 0 },
            { where: { id: id } }
        );

        return res.status(200).json({
            success: true,
            message: 'User inactive successfully.',
            data: user
        });
    } catch (error) {
        res.status(400).json('Error ocurred in user inactive.');
        next(error);
    }
}

const setImgProfile = async (req, res, next) => {
    const { fieldname, filename } = req.file;
    const { id } = req.params;

    const user = await User.findOne({ where: { id: id } });

    if( user.avatar !== '' ) {
        //Borramos la imagen antigua.
        fs.unlink(`./assets/profile/${user.avatar}`, (err) => {
            if (err) {
                console.log("failed to delete local image:"+err);
            } else {
                console.log('successfully deleted local image');                                
            }
        });

        /** fs comienza a partir del directorio raiz del proyecto node, por eso hacemos referencia a q iniciamos en el directorio actual y luego pasamos a las carpetas respectivas.  */
    }

    try {
        await User.update(
            { avatar: filename },
            { where: { id: id } }
        );

        return res.status(200).json({
            success: true,
            message: 'Set user avatar successfully.',
        });
        
    } catch (error) {
        res.status(400).json('Error ocurred in set user avatar.');
        next(error);
    }
}

module.exports = {
    index,
    login,
    register,
    getUser,
    updateUser,
    setActiveUser,
    setInactiveUser,
    setImgProfile
};