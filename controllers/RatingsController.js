const { Rating } = require('../models');

const store = async (req, res, next) => {
    const { product_id, user_id, value } = req.body;

    const data = {
        product_id,
        user_id,
        value,
        status: 1 // 0 Comment not aproved, 1 comment aproved.
    }

   const result = await upsert(data, { product_id, user_id });
    
    if(result.success) {
        return res.status(200).json({
            success: result.success,
            data: result.data
        });
    } else {
        return res.status(400).json({
            success: result.success,
            err: result.err
        });
    }
}

const upsert = async (values, condition) => {
    try {
        const res = await Rating.findOne({ where: condition });

        if(res) {
            await Rating.update(values, { where: condition });

            return {
                success: true,
                data: 'Thanks for supplied updated your valoration.'
            }

        } else {
            // insert
            await Rating.create(values);

            return {
                success: true,
                data: 'Thanks for supplied your valoration.'
            }
        }
    } catch (error) {
        return {
            success: false,
            err: 'Thanks for supplied your valoration.'
        }
    }
}

module.exports = {
    store,
}