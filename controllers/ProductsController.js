const { Sequelize } = require('sequelize');
const { Product, Category, Comments, User, Rating, ProductsPic } = require('../models/');
const fs = require('fs');

const index = async (req, res, next) => {
    try {
        const response = await Product.findAll({ 
            include: [ 
                { as: 'category', model: Category, attributes: ['name'] } ,
                { 
                    as: 'comments', 
                    model: Comments, 
                    required: false,
                    where: { status: 1 },
                    include: [ { as: 'user', model: User, attributes: ['email'] } ]
                },
            ],
        });

        //await Promise.all because async functions always return a Promise, but .map does not "await" these promises.
        let products = await Promise.all(
            response.map(async (val) => {
                let sum = await sumRatingsByID(val.id)
                let count = await countRatingsByID(val.id)
    
                return {
                    ...val.toJSON(),
                    ratingcount: count,
                    ratingsum: sum[0].total ? sum[0].total : 0
                }
            })
        );

        return res.status(200).json({
            success: true,
            data: products,
            message: 'List products availables.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const countRatingsByID = async (id) => {
    const count = await Rating.count({ where: { product_id:  id} })
    return count;
}

const sumRatingsByID = async (id) => {
    const total = await Rating.findAll({ 
        where: { product_id: id},
        attributes: [
            [Sequelize.fn('sum', Sequelize.col('value')), 'total']
        ],
        raw: true
    });

    return total;
}

const store = async (req, res, next) => {
    const { name, category_id, price, description } = req.body;

    const data = {
        name,
        category_id,
        price,
        description
    }

    try {
        await Product.create(data);

        return res.status(200).json({
            success: true,
            message: 'Product stored successfully.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const show = async (req, res, next) => {
    const { id } = req.params;

    try {
        const product = await Product.findOne({ where: { id } });

        return res.status(200).json({
            success: true,
            data: product,
            message: 'Product by id'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const update = async (req, res, next) => {
    const { name, category_id, price, description } = req.body;
    const { id } = req.params;

    const data = {
        name,
        category_id,
        price,
        description
    }

    try {
        await Product.update(
            data,
            { where: { id } }
        );

        return res.status(200).json({
            success: true,
            message: 'Product updated successfully.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const destroy = async (req, res, next) => {
    const { id } = req.params;

    try {
        await Product.destroy({ where: { id } });

        return res.status(200).json({
            success: true,
            message: 'Product deleted.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const uploadImage = async (req, res, next) => {
    const { filename } = req.file;
    const { id } = req.params;

    try {
        await ProductsPic.create({
            product_id: id,
            name: filename
        });

        return res.status(200).json({
            success: true,
            message: 'Add image to product successfully.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

const deleteImages = async (req, res, next) => {
    const { images } = req.body;
    const { id } = req.params;

    try {
        for (let i = 0; i < images.length; i++) {
            const count = await ProductsPic.count({ where: { name:  images[i]} })
            if(count > 0) {
                await ProductsPic.destroy({ where: { product_id: id,  name: images[i]} });
                await fs.unlink(`./assets/products/${images[i]}`, (err) => {
                    console.log(err);
                });
            }
        }

        return res.status(200).json({
            success: true,
            message: 'delete images from products successfully.'
        });

    } catch (error) {
        res.status(400).json({ err: error });
        next(error);
    }
}

module.exports = {
    index,
    store,
    show,
    update,
    destroy,
    uploadImage,
    deleteImages
}