let multer = require('multer');
const { randonmCharacters } = require('../helpers/helper');

let storage = multer.diskStorage({
    destination: (req, res, cb) => {
        cb(null, '../commercesv/assets/products');
    },
    filename: (req, file, cb) => {
        let ext = '';
        if(file.mimetype == 'image/jpeg') {
            ext = '.jpg'
        }
        
        if(file.mimetype == 'image/png') {
            ext = '.png'
        }

        cb(null, randonmCharacters(12) + ext);
    }
});

let upload = multer({ storage: storage });

module.exports = upload;