let multer = require('multer');
let storage = multer.diskStorage({
    destination: (req, res, cb) => {
        cb(null, '../commercesv/assets/profile');
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    }
});

let upload = multer({ storage: storage });

module.exports = upload;