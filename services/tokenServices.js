const jwt = require('jsonwebtoken');
const { User } = require('../models');

const SECRETKEY = '123456';

const decode = async (token) => {
    try {
        const { id } = await jwt.verify(token, SECRETKEY);
        const user = await User.findOne({ where: { id } });

        if(!user) {
            return false;
        }

        return user;

    } catch(e) {
        return false;
    }
}

module.exports = { decode }