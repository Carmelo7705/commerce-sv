'use strict';
const jwt = require('jsonwebtoken');

const SECRETKEY = '123456';

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User.init({
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    avatar: DataTypes.STRING,
    role: DataTypes.INTEGER,
    status: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User',
  });

  User.createToken = ({ id, firstname, email, avatar, role }) => {
    const payload = {
      id,
      firstname,
      email,
      avatar,
      role,
    };

    const token = jwt.sign(payload, SECRETKEY, { expiresIn: '3h' });
    return token;
  };

  User.refreshToken = ({ id }) => {
    const payload = {
      id,
    };

    const refreshToken = jwt.sign(payload, SECRETKEY, { expiresIn: '3d' });
    return refreshToken;
  };


  return User;
};