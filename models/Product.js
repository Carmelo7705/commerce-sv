'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Product.belongsTo(models.Category, { 
        as: 'category', 
        foreignKey: 'category_id',
      });

      Product.hasMany(models.Comments, {
        as: 'comments',
        foreignKey: 'product_id'
      });

      Product.hasMany(models.Rating, { 
        as: 'ratings', 
        foreignKey: 'product_id',
      });
    }
  };
  Product.init({
    name: DataTypes.STRING,
    category_id: DataTypes.INTEGER,
    price: DataTypes.FLOAT,
    description: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Product',
  });
  return Product;
};