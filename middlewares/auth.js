const TokenService = require('../services/tokenServices');

const auth__Admin = async (req, res, next) => {
    let token = req.headers.token;

    if(!token) {
        return res.status(404).json({
            message: 'Token not found.'
        });
    }

    const { role } = await TokenService.decode(token);

    let = rolesValid = [1];

    if(rolesValid.includes(role)) {
        next();
    } else {
        return res.status(403).json({
            message: 'Unauthorized.'
        });
    }
}

const auth_client = async (req, res, next) => {
    let token = req.headers.token;

    if(!token) {
        return res.status(404).json({
            message: 'Token not found.'
        });
    }

    const { role } = await TokenService.decode(token);

    let = rolesValid = [1,2];

    if(rolesValid.includes(role)) {
        next();
    } else {
        return res.status(403).json({
            message: 'Unauthorized.'
        });
    }
}

module.exports = {
    auth__Admin,
    auth_client
}