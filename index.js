const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');

require('dotenv').config();

const app = express();

//Rutas
const routes = require('./routes/api');

app.set('PORT', process.env.PORT || 3000);

app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//prefix routes
app.use('/', routes);


app.listen(app.get('PORT'), () => {
	console.log(`Servidor listo en puerto ${app.get('PORT')}`);
});